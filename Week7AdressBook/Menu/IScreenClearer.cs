﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdressBook
{
    public interface IScreenClearer
    {
        void ClearScreen();
    }
}
