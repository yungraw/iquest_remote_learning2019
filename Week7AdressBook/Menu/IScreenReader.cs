﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdressBook
{
    public interface IScreenReader
    {
        string ReadInput();
        int ReadInputInteger();
        Person ReadPerson();
    }
}
