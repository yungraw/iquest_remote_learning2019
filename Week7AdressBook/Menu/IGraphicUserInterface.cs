﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdressBook
{
    public interface IGraphicUserInterface : IScreenReader, IScreenPrinter, IScreenClearer
    {
        void AwaitUserInput();     
    }
}
