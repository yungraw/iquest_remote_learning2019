﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdressBook
{
    public interface IScreenPrinter
    {
        void PrintMessage(string message);
        void PrintMessageOnLine(string message);
        void PrintPerson(Person person);
    }
}
