﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Starbucks
{
    interface IDrink
    {
        void ChooseSize(string size);
    }
}
