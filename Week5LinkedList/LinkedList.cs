﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists
{
    class LinkedList : IEnumerable
    {
        public Node Head { get; set; }

        public LinkedList()
        {

        }


        public void addNode(object data)
        {
            Node newNode = new Node(data);

            if (newNode != null)
            {
                if (this.Head == null)
                {
                    this.Head = newNode;
                }
                else
                {
                    Node temp = this.Head;
                    this.Head = newNode;
                    newNode.Next = temp;
                }
            }
        }


        public void printNodes()
        {
            Node current = Head;
            while (current != null)
            {
                Console.WriteLine(current.Data);
                current = current.Next;
            }
        }

        public void removeNode(int nodeNumber)
        {

            Node current = this.Head;
            Node prev = null;

            int index = 0;
            while (index < nodeNumber && current != null)
            {
                prev = current;
                current = current.Next;

                index++;
            }

            if (current != null)
            {
                if (prev == null)
                {
                    this.Head = current.Next;
                }
                else
                {
                    prev.Next = current.Next;
                    current = null;
                }
                current = null;
            }
        }


        public bool VerifyElement(object Element)
        {
            Node current = Head;
            while (current != null)
            {
                if (current.Data == Element)
                {
                    Console.WriteLine("Element exists in the list.");
                    return true;
                }
                current = current.Next;
            }
            Console.WriteLine("Element doesnt exist in the list.");
            return false;


        }

        public Node BubbleSort(Node Head)
        {

            Node upperNode;
            Node lowerNode = null;
            bool isSwapped;

            if (Head == null)
            {

                return null;
            }

            do
            {
                isSwapped = false;
                upperNode = Head;

                while (upperNode.Next != lowerNode)
                {

                    if ((int)upperNode.Data > (int)upperNode.Next.Data )
                    {

                        int t = (int) upperNode.Data;
                        upperNode.Data = upperNode.Next.Data;
                        upperNode.Next.Data = t;
                        isSwapped = true;

                    }
                    upperNode = upperNode.Next;

                }
                lowerNode = upperNode;

            }

            while (isSwapped != false);

            return Head;

        }

        public IEnumerator GetEnumerator()
        {
            return new ListEnumerator(Head);
        }

    }
}
