﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists
{
    class ListEnumerator : IEnumerator
    {
        private Node current;
        private Node Head;

        public ListEnumerator(Node Head)
        {
            this.Head = Head;
            current = Head;
        }

        public bool MoveNext()
        {
            if(current != null)
            {
                return true;

            }
            else
            {
                return false;
            }
        }

        public object Current
        {
            get
            {
                object simpleNodeData = current.Data;
                current = current.Next;
                return simpleNodeData;
            }

        }

        public void Reset()
        {
            current = Head;
        }

    }
}
