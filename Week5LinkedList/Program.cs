﻿using System;

namespace LinkedLists
{
    class Program
    {
        static void Main(string[] args)
        {
   
            LinkedList myList = new LinkedList();
            Console.WriteLine("Simple Linked List");
            object obj1 = 1;
            object obj2 = 2;
            object obj3 = 3;
            object obj4 = 4;

            myList.addNode(obj1);
            myList.addNode(obj2);
            myList.addNode(obj3);
            myList.addNode(obj4);
            Console.WriteLine("Printing nodes..");
            myList.printNodes();

            myList.removeNode(2);
            Console.WriteLine("Node deleted!");
            Console.WriteLine("Printing nodes..");
            myList.printNodes();
            Console.WriteLine("Verifying if 3 is present..");
            myList.VerifyElement(obj3);
            Console.WriteLine("Running sort..");
            myList.BubbleSort(myList.Head);
            Console.WriteLine("Printing nodes..");
            myList.printNodes();
            Console.WriteLine("Printing nodes using foreach..");
            foreach (var simpleElement in myList)
            {
                Console.WriteLine(simpleElement);
            }
            Console.WriteLine("____________________________________________________");

            DoubleLinkedList myDoubleLinkedList = new DoubleLinkedList();
            Console.WriteLine("Double Linked List");
            myDoubleLinkedList.addNode(obj1);
            myDoubleLinkedList.addNode(obj2);
            myDoubleLinkedList.addNode(obj3);
            myDoubleLinkedList.addNode(obj4);
            Console.WriteLine("Printing nodes..");
            myDoubleLinkedList.printNodes();
            myDoubleLinkedList.removeNode(2);
            Console.WriteLine("Node deleted!");
            Console.WriteLine("Printing nodes..");
            myDoubleLinkedList.printNodes();
            Console.WriteLine("Verifying if 2 is present..");
            myDoubleLinkedList.VerifyElement(obj2);
            Console.WriteLine("Running sort..");
            myDoubleLinkedList.Head = myDoubleLinkedList.BubbleSort(myDoubleLinkedList.Head);
            Console.WriteLine("Printing nodes..");
            myDoubleLinkedList.printNodes();
            Console.WriteLine("Printing nodes using foreach..");
            foreach (var doubleElement in myDoubleLinkedList)
            {
                Console.WriteLine(doubleElement);
            }



        }
    }
}
