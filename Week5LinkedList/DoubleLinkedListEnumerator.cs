﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists
{
    class DoubleLinkedListEnumerator: IEnumerator
    {
        private DoubleNode current;
        private DoubleNode Head;

        public DoubleLinkedListEnumerator(DoubleNode Head)
        {
            this.Head = Head;
            current = Head;
        }

        public bool MoveNext()
        {
            if (current != null)
            {
                return true;

            }
            else
            {
                return false;
            }
        }

        public object Current
        {
            get
            {
                object nodeData = current.Data;
                current = current.Next;
                return nodeData;
            }

        }

        public void Reset()
        {
            current = Head;
        }

    }
}
