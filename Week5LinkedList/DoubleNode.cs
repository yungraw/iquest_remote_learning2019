﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists
{
    class DoubleNode
    {
        public object Data { get; set; }
        public DoubleNode Next { get; set; }
        public DoubleNode Previous { get; set; }


        public DoubleNode(object Data)
        {
            this.Data = Data;
        }


    }
}
