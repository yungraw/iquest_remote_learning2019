﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace LinkedLists
{
    class DoubleLinkedList
    {
        public DoubleNode Head { get; set; }

        public DoubleLinkedList()
        {

        }

        public void addNode(object data)
        {

            DoubleNode newNode = new DoubleNode(data);
            newNode.Next = this.Head;
            newNode.Previous = null;

            if (newNode != null)
            {
                
                if (this.Head != null)
                {
                    this.Head.Previous = newNode;
                }
                this.Head = newNode;
            }
        }

        public void printNodes()
        {
            DoubleNode current = Head;
            while (current != null)
            {
                Console.WriteLine(current.Data);
                current = current.Next;
            }
        }


        public void removeNode(int nodeNumber)
        {

            DoubleNode current = this.Head;
            DoubleNode prev = null;

            int index = 0;
            while (index < nodeNumber && current != null)
            {
                prev = current;
                current = current.Next;

                index++;
            }

            if (current != null)
            {
                if (prev == null)
                {
                    this.Head = current.Next;
                }
                else
                {
                    prev.Next = current.Next;
                    current = null;
                }
                current = null;
            }
        }

        public bool VerifyElement(object Element)
        {
            DoubleNode current = Head;
            while (current != null)
            {
                if (current.Data == Element)
                {
                    Console.WriteLine("Element exists in the list.");
                    return true;
                }
                current = current.Next;
            }
            Console.WriteLine("Element doesnt exist in the list.");
            return false;


        }

        public DoubleNode BubbleSort(DoubleNode Head)
        {

            DoubleNode upperNode;
            DoubleNode lowerNode = null;
            bool isSwapped;

            if (Head == null)
            {

                return null;
            }

            do
            {
                isSwapped = false;
                upperNode = Head;

                while (upperNode.Next != lowerNode)
                {

                    if ( (int) upperNode.Data >  (int) upperNode.Next.Data)
                    {

                        int t = (int)upperNode.Data;
                        upperNode.Data = upperNode.Next.Data;
                        upperNode.Next.Data = t;
                        isSwapped = true;

                    }
                    upperNode = upperNode.Next;

                }
                lowerNode = upperNode;

            }

            while (isSwapped != false);

            return Head;

        }

        public IEnumerator GetEnumerator()
        {
            return new DoubleLinkedListEnumerator(Head);
        }

    }
}
