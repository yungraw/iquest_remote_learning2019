﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1Animals
{
    class CatBehaviour : AnimalBehaviour
    {
        public CatBehaviour() : base()
        {

        }

        public CatBehaviour(string Name) : base(Name)
        {
            

        }


        public virtual void Stretch()
        {
            Console.WriteLine(Name + " stretches her body.");
        }

        public virtual void Meows()
        {
            Console.WriteLine(Name + " meows loudly.");
        }

        public virtual void Jump()
        {
            Console.WriteLine(Name + " starts jumping.");
        }

        public override void DoSomething()
        {
            Meows();
            Stretch();
            Jump();
        }
    }
}
