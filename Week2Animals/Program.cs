﻿using System;
using System.Collections.Generic;

namespace Week1Animals
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var animals = new List<Animal>
            {
             new Dog("Rex", new DogBehaviour("Rex")),
             new Dog("Azorel", new DogBehaviour("Azorel")),
             new Dog("Iancu" , new DogBehaviour("Iancu")),
             new Cat("Max", new CatBehaviour("Max")),
             new Alpaca("Pablo", new AlpacaBehaviour("Pablo")),
             new Duck("Ducky", new DuckBehaviour("Ducky")),

            };

            
            
                        
            Console.WriteLine("\nFirst part:");

            foreach (var animal in animals)
            {
               animal.MakeSound();
            }

            

            Console.WriteLine("\nSecond part:");

            foreach (var animal in animals)
            {
                animal.DoMovement();
            }



            
            Console.WriteLine("\nPress any key to continue..");
            Console.ReadLine();



        }
    }
}
