﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Week1Animals
{
    abstract class AnimalBehaviour : IMovement
    { 

        public string Name { get; set; }

        public string MovementType { get; set; }

        

        public AnimalBehaviour()
        {
            this.Name = Name;
        }

        public AnimalBehaviour(string Name)
        {
            this.Name = Name;
        }

        public virtual void FirstAction()
        {
            Console.WriteLine(Name + " does first action.");
        }
        public virtual void SecondAction()
        {
            Console.WriteLine(Name + " does second action.");
        }
        public virtual void ThirdAction()
        {
            Console.WriteLine(Name +" does third action.");
        }

        public virtual void DoSomething()
        {
            FirstAction();
            SecondAction();
            ThirdAction();

        }

        

    }
}
