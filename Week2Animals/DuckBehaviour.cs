﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1Animals
{
    class DuckBehaviour : AnimalBehaviour
    {
        public DuckBehaviour() : base()
        {

        }

        public DuckBehaviour(string Name) : base(Name)
        {
            

        }

        public void Spread()
        {
            Console.WriteLine(Name + " spreads its wings.");

        }

        public void Look()
        {
            Console.WriteLine(Name + " looks through the sky.");
        }

        public void Fly()
        {
            Console.WriteLine(Name + " starts flying.");
        }


        public override void DoSomething()
        {
            Spread();
            Look();
            Fly();
        }
    }
}
