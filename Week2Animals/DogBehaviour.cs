﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1Animals
{
    class DogBehaviour : AnimalBehaviour
    {

        public DogBehaviour(string Name) : base(Name)
        {
             
        }

        public DogBehaviour() : base()
        {


        }


        public virtual void StretchPaws()
        {
            
            Console.WriteLine(Name + " stretches his paws.");
        }

        public virtual void LookForward()
        {
            Console.WriteLine(Name + " looks forward.");
        }

        public virtual void Run()
        {
            Console.WriteLine(Name + " starts running.");
        }

        public override void DoSomething()
        {
            StretchPaws();
            LookForward();
            Run();
        }


    }
}
