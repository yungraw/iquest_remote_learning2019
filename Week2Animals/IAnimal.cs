﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1Animals
{
    interface IAnimal
    {
        void MakeSound();
        void DoMovement();
    }
}
