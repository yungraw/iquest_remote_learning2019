﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1Animals
{
    interface IMovement
    {
        void DoSomething();
        void FirstAction();
        void SecondAction();
        void ThirdAction();
    }
}
