﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1Animals
{
    class Duck : Animal
    {
        private readonly DuckBehaviour behaviour;

        public Duck(string Name, DuckBehaviour behaviour) : base(Name, behaviour)
        {
            
            this.SoundType = "quac";
            this.behaviour = behaviour;

        }

        public override void DoMovement()
        {
            behaviour.DoSomething();


        }


    }
}
