﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1Animals
{
    class Lion : Cat
    {
        public Lion(string name) : base(name)
        {
            this.SoundType = "rawghr";
            this.MovementType = "leaps";
        }

        public override void Stretch()
        {
            base.Stretch();
        }

        public override void Meows()
        {
            Console.WriteLine(Name + " rages loudly.");
        }

        public override void Jump()
        {
            Console.WriteLine(Name + " starts leaping.");
        }



    }
}
