﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1Animals
{
    class Dog : Animal
    {
        private readonly DogBehaviour behaviour;

        public Dog(string Name, DogBehaviour behaviour) : base(Name, behaviour)
        {
            this.behaviour = behaviour; // The Dog behaviour is part of a Dog
            this.SoundType = "ham";
            
        }

        public override void DoMovement()
        {
            
            behaviour.DoSomething();
            
        }




    }
}
