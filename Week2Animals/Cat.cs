﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1Animals
{
    class Cat : Animal
    {
        public readonly CatBehaviour behaviour;
        

        public Cat(string Name, CatBehaviour behaviour) : base(Name, behaviour)
        {
            
            this.SoundType = "miau";
            this.behaviour = behaviour;
        }

        public override void DoMovement()
        {
            behaviour.DoSomething();
        }
    }
}
