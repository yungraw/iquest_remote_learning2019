﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1Animals
{
    abstract class Animal : IAnimal
    {

        protected string Name { get; set; }

        public string SoundType { get; set; }

        public readonly AnimalBehaviour behaviour;


        public Animal(string Name, AnimalBehaviour behaviour)
        {
            this.Name = Name;
            this.behaviour = behaviour;
            this.SoundType = SoundType;
        }

        public Animal(string Name, string soundType)
        {
            this.Name = Name;
            this.SoundType = soundType;
            
        }

        public virtual void MakeSound()
        {
            Console.WriteLine(Name + " makes " + SoundType + ".");
        }

        public virtual void DoMovement()
        {
            behaviour.DoSomething();
            
        }



    }

}
