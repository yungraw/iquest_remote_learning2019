﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1Animals
{
    class AlpacaBehaviour : AnimalBehaviour
    {

        public AlpacaBehaviour(string Name) : base(Name)
        {

        }

        public AlpacaBehaviour() : base()
        {

        }

        public virtual void Look()
        {
            Console.WriteLine(Name + " looks silly at me.");

        }

        public virtual void Sneeze()
        {
            Console.WriteLine(Name + " sneezes.");

        }

        public virtual void Levitate()
        {
            Console.WriteLine(Name + " levitates");
        }


        public override void DoSomething()
        {
            Look();
            Sneeze();
            Levitate();

        }
    }
}
