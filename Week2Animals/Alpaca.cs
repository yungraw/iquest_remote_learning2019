﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1Animals
{
    class Alpaca : Animal
    {
        public readonly AlpacaBehaviour behaviour;

        public Alpaca(string Name, AlpacaBehaviour behaviour) : base(Name, behaviour)
        {
             // the alpaca behaviour is part of an alpaca
            this.SoundType = "arrrgh";
            this.behaviour = behaviour;

        }

        public override void DoMovement()
        {
             
            behaviour.DoSomething();

        }






    }
}
