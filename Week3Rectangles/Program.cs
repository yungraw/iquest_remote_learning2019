﻿using System;

namespace Week2BoardShapes
{
    class Program
    {
        static void Main(string[] args)
        {

            Board board = new Board();

            //Creating rectangles
            var r1 = new Rectangle( 2, 2, 40, 40);

            //Computing perimeters and surfaces for different rectangles
            var r1Perimeter = r1.ComputePerimeter();
            var r1Surface = r1.ComputeSurface();
            Console.WriteLine("Rectangle 1 perimeter is {0} and surface is {1}", r1Perimeter, r1Surface);


            
            //Adding rectangles on the board

            board.Add(r1);
            board.Add(new Rectangle(2, 3, 20, 40));
            board.Edit(new Rectangle(5, 5, 20, 20), 1);
            board.Remove(1);
            Console.WriteLine("Rectangle removed!");
            board.GetRectanglesNumber();
            board.CalculateTotalSurface();
            board.CheckPointsNumber();
        }
    }
}
