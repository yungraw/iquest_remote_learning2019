﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week2BoardShapes
{
    interface IDrawable
    {
        double ComputeSurface();
        double ComputePerimeter();
        int ComputePointsNumber();
    }
}
