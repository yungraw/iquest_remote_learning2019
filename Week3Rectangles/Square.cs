﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week2BoardShapes
{
    struct Square : IDrawable
    {
        public int Length { get; set; }
        public Point FirstPoint;


        public Square(int x1, int y1, int lenght)
        {
            FirstPoint = new Point(x1, y1);
            Length = lenght;
        }

        public Square(Square s)
        {
            FirstPoint = s.FirstPoint;
            Length = s.Length;

        }


        public double ComputePerimeter()
        {
            double perimeter = Length * 4;
            return perimeter;
        }

        public double ComputeSurface()
        {
            double surface = Length * Length;
            return surface;      
        }

        public int ComputePointsNumber()
        {
            int points_number = 4;

            return points_number;
        }
    }
}
