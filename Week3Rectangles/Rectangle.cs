﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week2BoardShapes
{
    struct Rectangle : IDrawable
    {

        public int Length { get; set; }
        public int Width { get; set; }
        public Point FirstPoint { get; set; }

        public Rectangle(int X, int Y, int lenght, int width) 
        {
            FirstPoint = new Point(X , Y) ;
            Length = lenght;
            Width = width;
        }

        public Rectangle(Rectangle r)
        {
            FirstPoint = r.FirstPoint;
            Length = r.Length;
            Width = r.Width;

        }

        public double ComputeSurface()
        {
            int surface = (Length * Width);
            return surface;

        }

        public double ComputePerimeter()
        {
            int perimeter = (2 * Length + 2 * Width);
            return perimeter;
        }

        public int ComputePointsNumber()
        {
            int points_number = 4;

            return points_number;
        }
    
        
    }
}
