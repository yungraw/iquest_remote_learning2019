﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week2BoardShapes
{
    struct Triangle : IDrawable
    {   
        private Point FirstP { get; set; }
        private Point SecondP { get; set; }
        private Point ThirdP { get; set; }



        public Triangle(int x1, int y1, int x2, int y2, int x3, int y3)
        {
            FirstP = new Point(x1, y1);
            SecondP = new Point(x2, y2);
            ThirdP = new Point(x3, y3);
 
        }

        public Triangle(Triangle t)
        {
            FirstP = t.FirstP;
            SecondP = t.SecondP;
            ThirdP = t.ThirdP;
        }


        public double ComputeDistance(Point p1, Point p2)
        {

            double distance = Math.Sqrt ( Math.Pow ( p2.X - p1.X, 2) + Math.Pow( (p2.Y - p1.Y), 2)) ;
            return distance;
        }

        public double ComputePerimeter()
        {
            double firstDistance = ComputeDistance(FirstP, SecondP);
            double secondDistance = ComputeDistance(SecondP, ThirdP);
            double thirdDistance = ComputeDistance(ThirdP, FirstP);
            double perimeter = firstDistance + secondDistance + thirdDistance;
            return perimeter;
        }

        public double ComputeSurface()
        {
            double firstDistance = ComputeDistance(FirstP, SecondP);
            double secondDistance = ComputeDistance(SecondP, ThirdP);
            double thirdDistance = ComputeDistance(ThirdP, FirstP);

            double p = (firstDistance + secondDistance + thirdDistance) / 2; // Folosesc Heron (never thought i would use again)

            double surface = Math.Sqrt(p * (p - firstDistance) * (p - secondDistance) * (p - thirdDistance)); // radical din p pe langa p-a pe langa p - b pe langa p-c YEAH

            return surface;
        }

        public int ComputePointsNumber()
        {
            int points_number = 3;

            return points_number;
        }
    }
}
