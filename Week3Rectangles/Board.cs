﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week2BoardShapes
{
    class Board
    {
        private List<IDrawable> drawables;


        public Board()
        {
            drawables = new List <IDrawable>();
        }


        public void Add(IDrawable figure)
        {
            drawables.Add(figure);

        }

        public void Remove(int index)
        {
            drawables.RemoveAt(index);

        }

        public void EditRectangle(Rectangle r, int index)
        {
            drawables[index] =  new Rectangle(r);
        }

        public void EditTriangle(Triangle r, int index)
        {
            drawables[index] = new Triangle(r);
        }

        public void EditSquare(Square r, int index)
        {
            drawables[index] = new Triangle(r);
        }

        public int GetRectanglesNumber()
        {
            return drawables.Count;
        }

        public double CalculateTotalSurface()
        {
            double totalSurface = 0;
            foreach (IDrawable figure in drawables)
            {
                double surface = figure.ComputeSurface();
                totalSurface += surface;
            }

           return totalSurface;

        }

        public void CheckPointsNumber()
        {
            var totalPoints = 0;
            foreach(IDrawable figure in drawables)
            {
                int pointsNumber = figure.ComputePointsNumber();
                totalPoints += pointsNumber;
            }
            Console.WriteLine("Total number of points on the board: {0}", totalPoints);
        }



    }
}
